import numpy as np
import pandas as pd
from tqdm import tqdm
import codecs


def col_to_ids(col):
    d = dict()
    l = []
    idx = 0
    for id in col:
        if id not in d.keys():
            d[id] = idx
            l.append(id)
            idx += 1
    return np.asarray(list(map(lambda x: d[x], col))), l


def create_users_items(input_path="csv/search_history.csv", output_path="csv/users_items.csv"):
    data = pd.read_csv(input_path, low_memory=False, sep='\t')

    # users_items = pd.DataFrame(index=data['user_id'].unique(), columns=data['content'].unique())
    # users_items.to_csv("users_items.csv", sep='\t')

    frequent_users = data.groupby('user_id').filter(lambda x: x['user_id'].count() > 100)
    frequent_words = frequent_users.groupby('content').filter(lambda x: x['content'].count() > 100)
    print(len(frequent_users['user_id'].unique()))
    print(len(frequent_words['content'].unique()))
    users_items = pd.DataFrame(columns=frequent_words['content'].unique(), index=frequent_users['user_id'].unique())

    results = ["\t".join(["user_id", "content", "search_count"])]
    for idx in tqdm(users_items.index):
        user_data = frequent_users[frequent_users['user_id'] == idx]
        for col in users_items.columns:
            search_count = len(user_data[user_data['content'] == col])
            if search_count > 0:
                results.append("\t".join([idx, col, str(search_count)]))
    results = "\n".join(results)
    with codecs.open(output_path, "w") as f:
        f.write(results)


def create_interactions(input_path="csv/search_history.csv", output_path="csv/interactions.csv"):
    data = pd.read_csv(input_path, low_memory=False, sep='\t')

    # users_items = pd.DataFrame(index=data['user_id'].unique(), columns=data['content'].unique())
    # users_items.to_csv("users_items.csv", sep='\t')

    frequent_users = data.groupby('user_id').filter(lambda x: x['user_id'].count() > 1).drop(['created_at', 'type'],
                                                                                             axis=1)

    results = []
    for idx in tqdm(frequent_users['user_id'].unique()):
        user_data = frequent_users[frequent_users['user_id'] == idx]
        results.append(idx + "\t" + "\t".join([str(c) for c in user_data['content']]))
    results = "\n".join(results)
    with codecs.open(output_path, "w") as f:
        f.write(results)
