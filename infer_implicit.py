import argparse

from spotlight.interactions import Interactions
import pandas as pd
import data_utils
import torch

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_path', type=str, default="csv/search_history.csv",
                        help='data directory')
    parser.add_argument('--user_id', type=str, default='10039')
    parser.add_argument('--num_suggestions', type=int, default=5)
    parser.add_argument('--model_path', type=str, default="model/implicit.pt",
                        help='data directory')
    args = parser.parse_args()
    data = pd.read_csv(args.data_path, low_memory=False, sep='\t').drop(['created_at', 'type'], axis=1)

    print("Creating id tables")
    user_ids, user_list = data_utils.col_to_ids(data['user_id'])
    item_ids, item_list = data_utils.col_to_ids(data['content'])
    user_to_predict = user_list.index(args.user_id)
    implicit_model = torch.load(args.model_path)
    print("User id = {}".format(args.user_id))
    suggestions = implicit_model.predict(user_to_predict, item_ids=None)
    best_suggestions = suggestions.argsort()[-args.num_suggestions:][::-1]
    print("Suggestions are:\n{} ".format(data['content'][best_suggestions]))
