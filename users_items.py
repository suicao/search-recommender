import pandas as pd
from surprise import Reader
from surprise import SVD
from surprise import Dataset
from surprise import accuracy
from surprise.model_selection import KFold

df = pd.read_csv("users_items.csv", low_memory=False, sep='\t')
df['search_count'] = 5
# A reader is still needed but only the rating_scale param is requiered.
reader = Reader(rating_scale=(1, 5))

# The columns must correspond to user id, item id and ratings (in that order).
data = Dataset.load_from_df(df[['user_id', 'content', 'search_count']], reader)

# define a cross-validation iterator
kf = KFold(n_splits=2)

algo = SVD()

for trainset, testset in kf.split(data):
    # train and test algorithm.
    algo.fit(trainset)
    predictions = algo.test(testset)
    # Compute and print Root Mean Squared Error
    accuracy.rmse(predictions, verbose=True)

# for prediction in predictions:
#     print(prediction)