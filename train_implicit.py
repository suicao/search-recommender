import argparse

from spotlight.interactions import Interactions
import pandas as pd
import numpy as np
from spotlight.factorization.implicit import ImplicitFactorizationModel
import data_utils
import torch


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--num_epoch', type=int, default=5)
    parser.add_argument('--data_path', type=str, default="csv/search_history.csv",
                        help='data directory')
    parser.add_argument('--batch_size', type=int, default=512)
    parser.add_argument('--output_path', type=str, default="model/implicit.pt")
    args = parser.parse_args()
    data = pd.read_csv(args.data_path, low_memory=False, sep='\t').drop(['created_at', 'type'], axis=1)

    print("Creating id tables")
    user_ids, user_list = data_utils.col_to_ids(data['user_id'])
    item_ids, item_list = data_utils.col_to_ids(data['content'])
    print("Training")
    implicit_interactions = Interactions(user_ids, item_ids)
    print("Creating model...")
    implicit_model = ImplicitFactorizationModel(n_iter=args.num_epoch, batch_size=args.batch_size)
    print("Fitting...")
    implicit_model.fit(implicit_interactions, True)
    torch.save(implicit_model, args.output_path)
